#!/usr/bin/env bash

python -m util.generate_embeddings --input-folder ./data/Training --output-folder ./model/ --cache-folder ./model/embeddings-cache
python -m training.train -d ./data/Training -e ./model/embeddings-cache