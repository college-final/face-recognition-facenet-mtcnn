import argparse
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats


def parse_args():
    parser = argparse.ArgumentParser(
        "Script for visualising time elapsed in face recognition training."
    )
    parser.add_argument(
        "-t",
        "--time-path",
        required=True,
        help="Path to file with Time Log with numpy format.",
    )
    parser.add_argument(
        "-l",
        "--label-path",
        required=True,
        help="Path to file with Label or Path with numpy format.",
    )
    return parser.parse_args()


def load_data():
    args = parse_args()

    time_list, label = np.loadtxt(args.time_path, dtype=np.str), np.loadtxt(
        args.label_path, dtype=np.str
    )
    time_list = np.array(time_list)
    label = np.array(label)

    return time_list, label


def main():
    time_list, label_raw = load_data()

    label = []
    for x in label_raw:
        label.append(x.split("/")[-1])
    x = list(range(0, len(time_list)))

    plt.plot(x, time_list.astype("float"), "bo")
    plt.xticks(x, label, rotation="vertical")
    plt.grid(True)

    plt.show()


def statistics():
    time_list, _ = load_data()
    print(stats.describe(time_list.astype(np.float)))


if __name__ == "__main__":
    statistics()
