#!/usr/bin/env bash

from_dir=(./data/Backup/Pinterest-Image-Dataset/*)
to_dir=(./data/Training)

# for dir in "${from_dir[@]}"; do
#     if [ -d "$dir" ]; then
#         cp -r "$dir" "$to_dir"
#         python -m util.generate_embeddings --input-folder ./data/Training --output-folder ./model/ --cache-folder ./model/embeddings-cache
#     fi
# done

find "${from_dir[@]::3}" -maxdepth 1 -type d -printf '%h\0%d\0%p\n' | sort -t '\0' -n | awk -F '\0' '{print $3}' | while read dir; do
    cp -r "$dir" "$to_dir"
    python -m util.generate_embeddings --input-folder ./data/Training --output-folder ./model/ --cache-folder ./model/embeddings-cache
done
