import os
from torchvision.datasets.folder import (
    IMG_EXTENSIONS,
    has_file_allowed_extension,
    pil_loader,
)
import joblib
import argparse
import csv
from inference.util import draw_bb_on_img
from inference.constants import MODEL_PATH
from face_recognition import preprocessing
from timeit import default_timer as timer


def parse_args():
    parser = argparse.ArgumentParser(
        "Script for detecting and classifying faces on user-provided images. This script will process images from input"
        " folder, and save the output result to csv."
    )
    parser.add_argument(
        "--input-folder", required=True, help="Folder where input images are."
    )
    parser.add_argument(
        "--output-file",
        required=False,
        help="File where prediction result will be saved",
    )
    return parser.parse_args()


def recognise_faces(model, img):
    faces = model(img)
    # if faces:
    #     draw_bb_on_img(faces, img)
    return faces, img


def main():
    args = parse_args()
    model = joblib.load(MODEL_PATH)
    preprocess = preprocessing.ExifOrientationNormalize()

    if not args.output_file:
        args.output_file = "./output.csv"

    header = [
        "File Name",
        "Expected Result",
        "Result",
        "Expected = Result",
        "Confidence Level",
        "Time Elapsed",
    ]
    dataresult = []

    for fname in filter(
        lambda p: has_file_allowed_extension(p, IMG_EXTENSIONS),
        os.listdir(args.input_folder),
    ):
        start = timer()
        print("Processing {}...".format(fname), end="", flush=True)
        try:
            img = preprocess(pil_loader(os.path.join(args.input_folder, fname)))
            faces, img = recognise_faces(model, img)
            if faces:
                prediction = faces[0].top_prediction
                expectedresult = fname[:-9].lower()
                end = timer()
                time = end - start
                facedata = [
                    fname,
                    expectedresult,
                    prediction.label,
                    expectedresult == prediction.label,
                    prediction.confidence,
                    str(end - start),
                ]
                dataresult.append(facedata)
                print("Done")
            else:
                print("No faces, skipping")
        except:
            print("Recognizing error, skipping")

    with open(args.output_file, "w", encoding="UTF8", newline="") as f:
        writer = csv.writer(f)
        writer.writerow(header)
        writer.writerows(dataresult)


if __name__ == "__main__":
    main()
