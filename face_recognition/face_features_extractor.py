import torch
from facenet_pytorch import MTCNN, InceptionResnetV1
from torchvision import transforms
from . import preprocessing
from facenet_pytorch.models.utils.detect_face import extract_face


class FaceFeaturesExtractor:
    def __init__(self):
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        print("Running on device: {}".format(self.device))

        self.aligner = MTCNN(
            keep_all=True, thresholds=[0.6, 0.7, 0.9], device=self.device
        )
        self.facenet_preprocess = transforms.Compose(
            [preprocessing.FixedImageStandardization()]
        )
        # FaceNet dengan model Inception ResNet
        # digunakan dengam pretrained model vggface2
        self.facenet = InceptionResnetV1(pretrained="vggface2").eval().to(self.device)

    def extract_features(self, img):
        bbs, _ = self.aligner.detect(img)
        if bbs is None:
            # if no face is detected
            return None, None

        faces = torch.stack([extract_face(img, bb) for bb in bbs]).to(self.device)

        # Mendapatkan Embedding dari input gambar yang digunakan
        embeddings = self.facenet(self.facenet_preprocess(faces)).detach().cpu().numpy()

        return bbs, embeddings

    def __call__(self, img):
        return self.extract_features(img)
